# Main Nginx server configuration
# Best recommendations from ruhighload.com implemented

# Default Nginx user.
user www-data;

pid /var/run/nginx.pid;

# Number of worker processes. It's recommended to use 'auto' in latest versions.
worker_processes auto;

events {
    # Connection selection method. Use 'epoll' in Linux and 'kqueue' in FreeBSD
    use epoll;
    # Number of worker connections. Use number between 1024 and 4096 here.
    worker_connections 2048;
    # Allow web-server to accept as many connections as possible.
    multi_accept on;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    # We don't need it. It takes disk space and CPU time.
    access_log off;
    # Log critical situations only.
    error_log /var/log/nginx/error.log crit;
    # Default logs format
    log_format main '$remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent" "$http_x_forwarded_for"';

    # Keep alive connections to prevent losses on opening new ones.
    keepalive_timeout 30;
    keepalive_requests 100;

    # Don't keep slow clients
    reset_timedout_connection on;
    client_body_timeout 10;
    send_timeout 2;

    # Limit dataflow
    client_max_body_size 1m;

    # More efficient method to send data than read+write
    sendfile on;

    # Send the beginning of the file right with headers in one packet
    tcp_nodelay on;
    tcp_nopush on;

    # Use Gzip to compress data before send
    gzip on;
    gzip_disable "msie6";
    gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;

    # Allow cache
    open_file_cache max=200000 inactive=20s;
    open_file_cache_valid 30s;
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    # Include default host configuration
    include /etc/nginx/default.conf;

    # Use following directory to mount volume with config files
    include /etc/nginx/conf.d/*.conf;
}